drop table SystemRole CASCADE constraint;
drop table DeptInfo CASCADE constraint;
drop table LocInfo CASCADE constraint;
drop table CompInfo CASCADE constraint;
drop table Login CASCADE constraint;
drop table LookupMaster CASCADE constraint;
drop table LookupData CASCADE constraint;
drop table ticketInfo CASCADE constraint;

create table                   SystemRole
(
oid                           	varchar2(128)                           not null,
roleId                         	varchar2(128)                           not null,
roleDescription                	CLOB                                    not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                               default current_timestamp,
constraint                     	pk_SystemRole                           primary key    (oid),
constraint                     	uk_roleId_SystemRole                    unique         (roleId),
constraint                     	ck_status_SystemRole                    check          (status = 'Active' or status = 'Inactive')
);

create table                   DeptInfo
(
oid                           	varchar2(128)                           not null,
DeptId                         	varchar2(128)                           not null,
DeptName                		varchar2(256)                           not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                            	default current_timestamp,
constraint                     	pk_DeptInfo                          	primary key    (oid),
constraint                     	uk_DeptId_DeptInfo                    	unique         (DeptId),
constraint                     	ck_status_DeptInfo                    	check          (status = 'Active' or status = 'Inactive')
);

create table                   LocInfo
(
oid                           	varchar2(128)                           not null,
LocId                         	varchar2(128)                           not null,
LocName                			varchar2(256)                           not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                             	default current_timestamp,
constraint                     	pk_LocInfo                           	primary key    (oid),
constraint                     	uk_LocIdLocInfo                    		unique         (LocId),
constraint                     	ck_status_LocInfo                   	check          (status = 'Active' or status = 'Inactive')
);

create table                   CompInfo
(
oid                           	varchar2(128)                           not null,
compId                         	varchar2(128)                           not null,
LocName                			varchar2(256)                           not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                             	default current_timestamp,
constraint                     	pk_CompInfo                           	primary key    (oid),
constraint                     	uk_compId_CompInfo                    	unique         (compId),
constraint                     	ck_status_CompInfo                   	check          (status = 'Active' or status = 'Inactive')
);

create table                   	Login
(
oid                            	varchar2(128)                            not null,
loginId                        	varchar2(128)                            not null,
password                       	varchar2(128)                            not null,
name                           	varchar2(256),
designation					   	varchar2(256),
mobileNo                       	varchar2(16)							not null,
address                        	varchar2(200),
email                          	varchar2(256),
status                         	varchar2(32)                             not null,
roleOid                        	varchar2(128)                            not null,
deptOid                        	varchar2(128)                            not null,
locOid                        	varchar2(128)                            not null,
compOid                        	varchar2(128)                            not null,
createdBy                      	varchar2(128)                            default 'System'			not null,
createdOn                      	timestamp                               default current_timestamp	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp,
constraint                     	pk_Login                                primary key    (oid),
constraint                     	ck_status_Login                         check          (status = 'Active' or status = 'Inactive'),
constraint                     	fk_roleOid_Login                        foreign key    (roleOid) references     SystemRole(oid),
constraint                     	fk_deptOid_Login                        foreign key    (deptOid) references     DeptInfo(oid),
constraint                     	fk_locOid_Login                        foreign key    (locOid) references     LocInfo(oid),
constraint                     	fk_compOid_Login                        foreign key    (compOid) references     CompInfo(oid)
);

create table                   LookupMaster
(
oid                           	varchar2(128)                           not null,
LooupId                         varchar2(128)                           not null,
LooupName                		varchar2(256)                           not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                             	default current_timestamp,
constraint                     	pk_LookupMaster                         primary key    (oid),
constraint                     	uk_LooupId_LookupMaster                 unique         (LooupId),
constraint                     	ck_status_LookupMaster                  check          (status = 'Active' or status = 'Inactive')
);
create table                   LookupData
(
oid                           	varchar2(128)                           not null,
LooupDataId                    	varchar2(128)                           not null,
LookupDataDesc					varchar2(256)                           not null,
status                         	varchar2(32)                            not null,
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                             	default current_timestamp,
constraint                     	pk_LookupData                           primary key    (oid),
constraint                     	uk_LooupDataId_LookupData               unique         (LooupDataId),
constraint                     	ck_status_LookupData                   	check          (status = 'Active' or status = 'Inactive'),
constraint                     	fk_LooupDataId_LookupData               foreign key    (LooupDataId) references     LookupMaster(oid)
);
create table                   ticketInfo
(
oid                           	varchar2(128)                           not null,
ticketId                        varchar2(128)                       not null,
ticketTitle                		varchar2(256)                       not null,
ticketDesc                      varchar2(32)                        not null,
prID							varchar2(128)                       not null, -- Priroty
Status							varchar2(128)						not null,
ActionStatus					varchar2(128)						not null, -- Ticket status
compId                         	varchar2(128)                       not null,
ApprovalReq						varchar2(10), -- Yes/No
OnbehOf							varchar2(128),
Remarks							varchar2(256),
AttachmetState					varchar2(10), -- Yes/No
createdBy                      	varchar2(128)                           default 'System'				not null,
createdOn                      	timestamp                               default current_timestamp 	not null,
updatedBy                      	varchar2(128),
updatedOn                      	timestamp                             	default current_timestamp,
constraint                     	pk_ticketInfo                           	primary key    (oid),
constraint                     	fk_compId_ticketInfo                   foreign key    (compId) references     CompInfo(oid),
constraint                     	fk_OnbehOf_ticketInfo               	foreign key    (OnbehOf) references     Login(oid),
constraint                     	fk_prID_ticketInfo               		foreign key    (prID) references     LookupData(oid),
constraint                     	fk_ActionStatus_ticketInfo              foreign key    (ActionStatus) references LookupData(oid),
constraint                     	ck_status_ticketInfo                   	check          (status = 'Active' or status = 'Inactive')
);
